Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :users, only: [:create] do
    resources :posts, only: [:create] 
    resources :comments, only: [:create]
  end

  resources :newsfeed, only: [] do
    collection do
      get :news_feed
    end
  end
end
