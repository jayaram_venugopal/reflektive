class User < ApplicationRecord
  has_many :posts
  has_many :comments

  validates_uniqueness_of :name, message: "must be unique"
end
