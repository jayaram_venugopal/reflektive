class Post < ApplicationRecord
  belongs_to :user
  has_many :comments

  default_scope { order(created_at: :desc) }

  scope :with_user_and_comments, -> { includes(:user, :comments => :user)}
end
