class UsersController < ApplicationController
  
  def create
    @user = User.create!(create_params)
    json_response(@user, :created)
  end

  private
  def create_params
    params.require(:user).permit(:name)
  end
end
