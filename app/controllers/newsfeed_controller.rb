class NewsfeedController < ApplicationController
  
  def news_feed
    @posts = Post.all
    render json: @posts, each_serializer: PostSerializer
  end

end
