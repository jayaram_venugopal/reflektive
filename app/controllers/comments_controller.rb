class CommentsController < ApplicationController
  before_action :set_post, only: [:create]
  before_action :set_current_user, only: [:create]

  def create
		@comment = @user.comments.new(comment_params)
    @comment.post_id = @post.id
    @comment.save
    json_response(@comment.as_json, :created)
  end

  private
  def comment_params
    params.require(:comment).permit(:content)
  end

  def set_current_user
    @user = User.find(params[:user_id])
  end

  def set_post
    @post = Post.find(params[:post_id])
  end
  
end
