class PostsController < ApplicationController
  def create
    @post = current_user.posts.create!(post_params) 
    json_response(@post.as_json, :created)
  end

  private
  def post_params
    params.require(:post).permit(:content)
  end

  def current_user
    User.find(params[:user_id])
  end
end