class PostSerializer < ActiveModel::Serializer
  has_one :user, serializer: UserSerializer
  has_many :comments, serializer: CommentSerializer
  
  attributes :type, :content

  private
  def type
    object.class.to_s
  end

end
