class CommentSerializer < ActiveModel::Serializer
  has_one :user, serializer: UserSerializer
  has_one :post, serializer: PostSerializer
  
  attributes :type, :user, :content

  def user
    UserSerializer.new(object.user, root: false).as_json
  end
  
  private
  def type
    object.class.to_s
  end
end