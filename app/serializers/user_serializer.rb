class UserSerializer < ActiveModel::Serializer
  has_many :posts, serializer: PostSerializer
  has_many :comments, serializer: CommentSerializer
  
  attributes :type, :name

  def as_json
    json = {
      :type => type,
      :name => name
    }
    json
  end

  private
  def type
    object.class.to_s
  end


  def name
    object.name
  end
end
