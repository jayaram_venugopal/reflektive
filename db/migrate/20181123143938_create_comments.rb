class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.references :user,           null: false, index: true
      t.references :post,           null: false, index: true
      t.text :content,              null: false
      t.timestamps
    end
  end
end
